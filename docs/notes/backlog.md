# Backlog

Online development environment

* Gitpod
* GitHub Codespaces

Orchestrating

* GitHub Actions
* GitLab CI
* Jenkins (Jenkinsfile)
* AWS code pipeline
* Azure DevOps

* Chef
* Ansible
* Puppet

* Rundeck
