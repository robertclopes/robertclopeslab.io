# DJANGO NOTES

## Comandos Python

```python
source venv/Scripts/activate

pip install -r requirements.txt
pipdeptree > requirements.txt

```

## Comandos Django

```python
django-admin startproject core
mv core src
cd src
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
django-admin startapp app 
python manage.py startapp app

```
