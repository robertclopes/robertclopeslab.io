# Create SSH Login banner

I use to save notes with users and passwords in my lab environments.

1. Go to the following website to create your ASCII banner -> [Text to ASCII](http://patorjk.com/software/taag/#p=display&f=ANSI%20Shadow&t=LAB){target=_blank}

2. Copy the converted ASCII to your clipboard.

3. Log into the desired server as root

4. Create /etc/banner file if not present and add your ASCII art to it and other notes
```bash
██╗      █████╗ ██████╗
██║     ██╔══██╗██╔══██╗
██║     ███████║██████╔╝
██║     ██╔══██║██╔══██╗
███████╗██║  ██║██████╔╝
╚══════╝╚═╝  ╚═╝╚═════╝
USERS:
root:toor
oracle:oracle
```

5. Add the following to the ssh config found in /etc/ssh/sshd_config
```bash
Banner /etc/banner
```

6. Restart SSH service via command
```bash
/etc/init.d/sshd restart
systemctl restart sshd
```

## References

<https://medium.com/sysops/how-to-create-ssh-login-banner-9fdb1856eb0f>