# PMM 2

## Install Server

```bash
yum install docker
systemctl enable docker
systemctl start docker

docker pull percona/pmm-server:2
mkdir /pmm-data
docker create --volume /pmm-data:/srv --name pmm-data percona/pmm-server:2 /bin/true

docker run --detach --restart always --publish 80:80 --publish 443:443 --volumes-from /pmm-data:/srv --name pmm-server percona/pmm-server:2
admin:admin-pmm

docker exec -it pmm-server curl -u admin:admin-pmm http://localhost/v1/version
```

## Install Client

```bash
yum install https://repo.percona.com/yum/percona-release-latest.noarch.rpm
yum install pmm2-client

pmm-admin config --metrics-mode=push --server-insecure-tls --server-url=https://admin:admin@<IP Address>:443

```

## Server Configurations

### STMP Service

```bash
docker exec -it pmm-server bash

grafana-cli plugins install grafana-image-renderer

vim /etc/grafana/grafana.ini
```

```bash
[smtp]
enabled = true
host = smtp.mail.com:587
user = <o365 username>
password = <o365 Password>
;cert_file =
;key_file =
skip_verify = false
from_address = < username>
from_name = Grafana
;EHLO identity in SMTP dialog (defaults to instance_name)
;ehlo_identity = dashboard.example.com
[emails]
;welcome_email_on_sign_up = false
```

```bash
supervisorctl restart grafana
```

## References

* [Documentation](https://www.percona.com/doc/percona-monitoring-and-management/2.x/index.html){target=_blank}
* [SMTP configuration](https://www.percona.com/blog/2020/07/07/enable-email-sending-in-percona-monitoring-and-management-2/){target=_blank}
