# Create a base Oracle Database

* Create or clone an base VM with OS

* [Oracle Database](https://www.oracle.com/br/database/technologies/oracle-database-software-downloads.html){target=_blank}

* [Sql Developer](https://www.oracle.com/database/technologies/appdev/sqldeveloper-landing.html){target=_blank}

## Details for a base VM

* Network

  * VirtualBox Host-Only Ethernet Adapter (10.0.0.10/255.255.255.0) For the VM interface use MAC (000000000010) and IP 10.0.0.10 changing the last number for you easy control

  * Bridge Interface (new random MAC)

## Details for install a base OS

* Check network main interface configuration

```bash
vim /etc/sysconfig/network-scripts/ifcfg-enp0s3
    TYPE=Ethernet
    PROXY_METHOD=none
    BROWSER_ONLY=no
    BOOTPROTO=static
    IPADDR=10.0.0.10
    NETMASK=255.255.255.0
    GATEWAY=10.0.0.1
    DEFROUTE=yes
    IPV4_FAILURE_FATAL=no
    IPV6INIT=NO
    NAME=enp0s3
    DEVICE=enp0s3
    ONBOOT=YES
systemctl restart network    
```

* Set ip/host to better response of internal services and remote connection

```bash
vim /etc/hosts
10.0.0.10 oracledatabase21c
```

* Update hostname

```bash
vim /etc/hostname
oracledatabase21c
```

* Create motd "Message Of The Day" to create notes about the Lab

```bash
vim /etc/motd
#Installed in this lab
# Oracle EE 21c
```

* Reboot

## Detail for install a Oracle Database VM

* Clone your base VM OS

* Install preinstall package

```bash
yum install oracle-database-preinstall-21c
passwd oracle

# Create a note of password for this lab
vim /etc/banner
oracle:oracle
```

* Download packages for share Folder

For this lab will use LINUX.X64_213000_db_home.zip and save into VM_SHARE folder in VirtualBox mounted in /VM_SHARE.

* Create base folders

```bash
mkdir -p /u01/app/oracle/product/19.0.0/dbhome_1
mkdir -p /u02/oradata
chown -R oracle:oinstall /u01 /u02
```

* Installing Oracle Database

```bash
cd /u01/app/oracle/product/19.0.0/dbhome_1
unzip -oq /VM_SHARE/LINUX.X64_213000_db_home.zip
chown oracle:oinstall /u01
su - oracle
cd /u01/app/oracle/product/19.0.0/dbhome_1
./runInstaller

# Install Listener
bin/netca
# Install Database
bin/dbca
# Change oradata to /u02
```

### Installing Sample Database

```bash
# as root
cd /VM_SHARE
git clone https://github.com/oracle/db-sample-schemas.git
# or
wget https://github.com/oracle/db-sample-schemas/archive/refs/tags/v21.1.zip

cp -R db-sample-schemas/ /home/oracle/
chown -R oracle:oinstall /home/oracle/db-sample-schemas/

# as oracle
su - oracle
cd $HOME/db-sample-schemas
perl -p -i.bak -e 's#__SUB__CWD__#'$(pwd)'#g' *.sql */*.sql */*.dat 
. oraenv
    [lab21c]
sqlplus system@pdblab21c
create tablespace SAMPLE;
@mksample Welcome123 Welcome123 hr_Welcome123 oe_Welcome123 pm_Welcome123 ix_Welcome123 sh_Welcome123 bi_Welcome123 SAMPLE TEMP $ORACLE_HOME/demo/schema/log/ 10.0.0.10:1521/pdblab21c

```

### Customize SQLPlus

SYNTAX
HIST[ORY] [n RUN | EDIT | DEL[ETE]] | [CLEAR | LIST]

Package needed for arrow navigation: *rlwrap*

alias rl_sqlplus='rlwrap sqlplus'
alias rl_rman='rlwrap rman'
alias rl_asmcmd='rlwrap asmcmd'

```bash
cd $ORACLE_HOME/sqlplus/admin
vim glogin.sql
set hist on
set pagesize 2000
set linesize 200
```

Make a snapshot or clone the VM with your database installation to do your activities.
