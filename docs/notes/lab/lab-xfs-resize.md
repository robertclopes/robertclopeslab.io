# RESIZE LVM(XFS )

```bash

    xfsdump -f /tmp/devmappertarget.dump /devmappertarget

umount /home
lvreduce -L -35.12G /dev/mapper/ol_oraclelinux7-home
mkfs.xfs -f /dev/mapper/ol_oraclelinux7-home
mount -a

    xfsrestore -f /tmp/devmappertarget.dump /mountofdevmappertarget


lvresize -L +35.12G /dev/mapper/ol_oraclelinux7-root
xfs_growfs /dev/mapper/ol_oraclelinux7-root
```