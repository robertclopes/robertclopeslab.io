# Create a base OS VM with latest OL

* [Oracle VM VirtualBox and Extension Pack](https://www.oracle.com/virtualization/technologies/vm/downloads/virtualbox-downloads.html){target=_blank}

* [Oracle Linux 7 or 8 (Download a Full ISO)](https://yum.oracle.com/oracle-linux-isos.html){target=_blank}

* [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)

* [Xming](http://www.straightrunning.com/XmingNotes/)

## Details for a base VM

* System
    Disable Floppy Disk

* Storage
    Create a disk VDI, minimum 100GB

* Disable Audio

* Network
  * VirtualBox Host-Only Ethernet Adapter (10.0.0.1/255.255.255.0) For the VM interface use MAC (000000000002) and IP 10.0.0.2, changing the last number for you easy control
  * Bridge Interface

* Disable USB

## Details for install a base OS

* Set NTP server
br.pool.ntp.org

* Install some tools packages 
<https://docs.oracle.com/cd/E37670_01/F14052/html/alternate.html>

```bash
yum install oracle-epel-release-el7 -y

yum update

yum install -y mlocate gcc make perl net-tools sysstat vim xclock xauth htop curl bzip2 unzip kernel-uek-devel-$(uname -r) git wget
```

* Install VBox Guest Additions

```Bash
mount /dev/cdrom /media
cd /media
./VBoxLinuxAdditions.run
reboot
```

* Check network main interface configuration

```bash
vim /etc/sysconfig/network-scripts/ifcfg-enp0s3
    TYPE=Ethernet
    PROXY_METHOD=none
    BROWSER_ONLY=no
    BOOTPROTO=static
    IPADDR=10.0.0.2
    NETMASK=255.255.255.0
    GATEWAY=10.0.0.1
    DEFROUTE=no
    IPV4_FAILURE_FATAL=no
    IPV6INIT=NO
    NAME=enp0s3
    DEVICE=enp0s3
    ONBOOT=YES
systemctl restart network    
```

* Set ip/host to better response of internal services and remote connection

```bash
vim /etc/hosts
    10.0.0.2 oraclelinux
```

* Create [banner](../lab-banner.md)

* Create motd "Message Of The Day" to create notes about the Lab

```bash
vim /etc/motd
# Installed just basic packages
```

## Create a Shared Folder for for VM

VBox Guest Additions need be installed

- Folder Path: E:\VM_SHARE
- Folder Name: VM_SHARE
- Check "Mount automatic"
- Mount Point: /VM_SHARE

```bash
# If not mounted automatic
mkdir /VM_SHARE
mount -t vboxsf E:\VM_SHARE /VM_SHARE
```

Let a file in folder E:\VM_SHARE to test(test.txt). When boot the system made a test in OS (touch test.md)

## Disable SELinux and Firewall to make more easy activities with Lab

```bash
systemctl stop firewalld
systemctl disable firewalld

vim /etc/selinux/config
SELINUX=disabled
```

## Test remote connection SSH + X

* With Putty and Xming installed, start Xming and open Putty, Enable X11 forwarding on Putty Configuration (Connection > SSH > X11)

* Connect with putty in 10.0.0.2:22 and test xclock

```bash
xclock
```
