# Vagrant

Ferramenta para provisionamento de ambiente virtual.
Documentação baseada em provisionar ambiente no Windows 10.

## Requisitos

Instalar um serviço provedor (**VirtualBox**, Hyper-V, Docker).

https://www.virtualbox.org/wiki/Downloads

Instalar o Vagrant.

https://www.vagrantup.com/downloads

## Principais comandos

```bash
# criar novo ambiente
vagrant init
# destruir um ambiente
vagrant destroy

# iniciar um ambiente pronto
vagrant up
# parar um ambiente (halt ou shutdown - o halt tenta um shutdown, caso falhe já força o desligamento)
vagrant halt
# reiniciar - The equivalent of running a halt followed by an up.
vagrant reload [name|id]

# conectar no ambiente
vagrant ssh

```
