# Creating a similar statics site for Documentation as a code

## Resources used

* Git
* GitLab Pages
* Markdown
* MkDocs (Material for MkDocs)
* VSCode
  * markdownlint
  * Draw.io Integration
  * PlantUML
  * Marp for VSCode
  * code-spell-checker
  * code-spell-checker-portuguese-brazilian
* markdown-link-check
* spellchecker-cli

## Local Environment

### Additional resources for MkDocs

```bash
# Install theme material
pip install mkdocs-material

# Install plugin to show date of last changes
pip install mkdocs-git-revision-date-plugin

# Install plugin that makes lists truly sane. Features custom indents for nested lists and fix for messy linebreaks and paragraphs between lists.  Custom indent for nested lists. Defaults to 2.
pip install mdx_truly_sane_lists

# To install the command line tool globally, run:
npm install -g markdown-link-check

# Start service
mkdocs serve
```

Values used in **mkdocs.yml** to format this env.

```yaml
plugins:
  - git-revision-date
markdown_extensions:
  - attr_list # allows to add HTML attributes and CSS classes to Markdown elements - exemplo colocar links para abrir em outra aba {target=_blank}
  - mdx_truly_sane_lists # makes lists truly sane.
  # Below there's 3 different extensions to set up syntax highlighting for code blocks
  - pymdownx.highlight
  - pymdownx.superfences
  - pymdownx.inlinehilite
theme:
  name: material # active theme material
  language: pt # set page language
```

### Publishing

Before commit and push check for broken links with markdown-link-check

```bash
# Check links from a local markdown folder (recursive), -q display erros only
find . -name \*.md -exec markdown-link-check -q {} \;
```

## References

* [ADDO - CI and CD for Documentation](https://content.sonatype.com/2020addo/addo2020-cicd-jacques-gil?lx=HfWkGj){target=_blank}
* [MkDocs](https://www.mkdocs.org/){target=_blank}
* [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/){target=_blank}
* [markdown-link-check](https://www.npmjs.com/package/markdown-link-check?activeTab=readme){target=_blank}
