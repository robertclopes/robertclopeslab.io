# OCP 19 - [Introdução](./dbaocm/dbaocm.md)

## Oracle Database Administration I 1Z0-082

### Understanding Oracle Database Architecture

* Understanding Oracle Database Instance Configurations <https://docs.oracle.com/en/database/oracle/oracle-database/19/cncpt/oracle-database-instance.html#GUID-1A8EA852-BC7F-4324-9318-ED3D72B4F127>
* Understanding Oracle Database Memory and Process Structures
* Understanding Logical and Physical Database Structures
* Understanding Oracle Database Server Architecture

### Managing Users, Roles and Privileges

* Assigning Quotas to Users
* Applying the Principal of Least Privilege
* Creating and Assigning Profiles
* Administering User Authentication Methods
* Managing Oracle Database Users, Privileges, and Roles

### Moving Data

* Using External Tables
* Using Oracle Data Pump
* Using SQL*Loader

### Configuring Oracle Net Services

* Using Oracle Net Services Administration Tools
* Configuring Communication Between Database Instances
* Configuring the Oracle Net Listener
* Connecting to an Oracle Database Instance
* Comparing Dedicated and Shared Server Configurations
* Administering Naming Methods

### Managing Undo

* Understanding Transactions and Undo Data
* Storing Undo Information
* Configuring Undo Rentention
* Comparing Undo Data and Redo Data
* Understanding Temporary Undo

### Using Conversion Functions and Conditional Expressions

* Applying the NVL, NULLIF, and COALESCE functions to data
* Understanding implicit and explicit data type conversion
* Using the TO_CHAR, TO_NUMBER, and TO_DATE conversion functions
* Nesting multiple functions

### Using SET Operators

* Matching the SELECT statements
* Using the ORDER BY clause in set operations
* Using The INTERSECT operator
* Using The MINUS operator
* Using The UNION and UNION ALL operators

### Managing Views

* Managing Views

### Retrieving Data using the SQL SELECT Statement

* Using Column aliases
* Using The DESCRIBE command
* Using The SQL SELECT statement
* Using concatenation operator, literal character strings, alternative quote operator, and the DISTINCT keyword
* Using Arithmetic expressions and NULL values in the SELECT statement

### Reporting Aggregated Data Using Group Functions

* Restricting Group Results
* Creating Groups of Data
* Using Group Functions

### Managing Tables using DML statements

* Managing Database Transactions
* Using Data Manipulation Language
* Controlling transactions

### Managing Schema Objects

* Creating and using temporary tables
* Managing constraints

### Managing Database Instances

* Starting Up Oracle Database Instances
* Using Data Dictionary Views
* Shutting Down Oracle Database Instances
* Using Dynamic Performance Views
* Using the Automatic Diagnostic Repository (ADR)
* Using the Alert Log and Trace Files
* Managing Initialization Parameter Files

### Managing Storage

* Managing Resumable Space Allocation
* Shrinking Segments
* Deferring Segment Creation
* Using Space-Saving Features
* Deploying Oracle Database Space Management Features
* Managing Different Types of Segments
* Using Table and Row Compression
* Understanding Block Space Management

### Accessing an Oracle Database with Oracle supplied Tools

* Using the Database Configuration Assistant (DBCA)
* Using Oracle Enterprise Manager Cloud Control
* Using racle enterprise Manager Database Express
* Using SQL Developer
* Using SQL Plus

### Managing Tablespaces and Datafiles

* Viewing Tablespace Information
* Creating, Altering and Dropping Tablespaces
* Managing Table Data Storage
* Implementing Oracle Managed Files
* Moving and Renaming Online Data Files

### Restricting and Sorting Data

* Applying Rules of precedence for operators in an expression
* Limiting Rows Returned in a SQL Statement
* Using Substitution Variables
* Using the DEFINE and VERIFY commands

### Displaying Data from Multiple Tables Using Joins

* Using Self-joins
* Using Various Types of Joins
* Using Non equijoins
* Using OUTER joins

### Understanding Data Definition Language

* Using Data Definition Language

### Managing Data in Different Time Zones

* Working with CURRENT_DATE, CURRENT_TIMESTAMP,and LOCALTIMESTAMP
* Working with INTERVAL data types

### Using Single-Row Functions to Customize Output

* Manipulating strings with character functions in SQL SELECT and WHERE clauses
* Performing arithmetic with date data
* Manipulating numbers with the ROUND, TRUNC and MOD functions
* Manipulating dates with the date function

### Using Subqueries to Solve Queries

* Using Single Row Subqueries
* Using Multiple Row Subqueries

### Managing Sequences, Synonyms, Indexes

* Managing Indexes
* Managing Synonyms
* Managing Sequences

## Oracle Database Administration II 1Z0-083

### Creating CDBs and Regular PDBs

* Configure and create a CDB
* Create a new PDB from the CDB seed
* Explore the structure of PDBs

### Backup and Duplicate

* Perform Backup and Recover CDBs and PDBs
* Duplicate an active PDB
* Duplicate a Database

### Manage Application PDBs

* Explain the purpose of application root and application seed
* Define and create application PDBs
* Install, upgrade and Patch applications
* Create and administer Application PDBS
* Clone PDBs and Application containers
* Plug and unplug operations with PDBs and application containers
* Comparing Local Undo Mode and Shared Undo Mode

### Recovery and Flashback

* Restore and Recovering Databases with RMAN
* Perform CDB and PDB flashback

### Backup Strategies and Terminology

* Perform Full and Incremental Backups and Recoveries
* Compress and Encrypt RMAN Backups
* Use a media manager
* Create multi-section backups of very large files
* Create duplexed backup sets
* Create archival backups
* Backup of recovery files
* Backup non database files
* Back up ASM meta data

### Restore and Recovery Concepts

* Employ the best Oracle Database recovery technology for your failure situation
* Describe and use Recovery technology for Crash, Complete, and Point-in-time recovry

### Using Flashback Technologies

* Configure  your Database  to support Flashback
* Perform flashback operations

### Duplicating a Database

* Duplicate Databases

### Diagnosing Failures

* Detect and repair database and database block corruption
* Diagnosing Database Issues

### Transporting Data

* Transport Data

### Install Grid Infrastructure and Oracle Database

* Install Grid Infrastructure for a Standalone server
* Install Oracle Database software

### Patching Grid Infrastructure and Oracle Database

* Patch Grid Infrastructure and Oracle Database

### Upgrading to Oracle Grid Infrastructure

* Upgrade Oracle Grid Infrastructure

### Oracle Database 18c: New Features

* Image and RPM based Database Installation

### Oracle Restart

* Configure and use Oracle Restart to manage components

### Install Grid Infrastructure for a Standalone server

* Rapid Home Provisioning

### Using Availability Enhancements

* Use an RMAN recovery catalog
* Use Flashback Database

### Monitoring and Tuning Database Performance

* Managing Memory Components
* Understanding The Automatic Workload Repository (AWR)
* Understanding The Advisory Framework
* Monitoring Wait Events, Sessions, and Services
* Managing Metric Thresholds and Alerts
* Understanding and Using The Performance Tuning Methodology
* Performing Performance Planning
* Understanding The Automatic Database Diagnostic Monitor (ADDM)

### Manage CDBs and PDBs

* Manage PDB service names and connections
* Manage startup, shutdown and availability of CDBs and PDBs
* Change the different modes and settings of PDBs
* Evaluate the impact of parameter value changes
* Performance management in CDBs and PDBs
* Control CDB and PDB resource usage with the Oracle Resource Manager

### Upgrading and Transporting CDBs and Regular PDBs

* Upgrade an Oracle Database
* Transport Data

### Manage Security in Multitenant databases

* Manage Security in Multitenant databases
* Manage PDB lockdown profiles
* Audit Users in CDBs and PDBs
* Manage other types of policies in application containers

### Configuring and Using RMAN

* Configure RMAN and the Database for Recoverability
* Configureand Using an RMAN recovery catalog

### Performing Recovery

* Restore and Recovering Databases with RMAN
* Perform Non RMAN database recovery

### RMAN Troubleshooting and Tuning

* Interpret the RMAN message output
* Diagnose RMAN performance issues

### Creating an Oracle Database by using DBCA

* Create, Delete and Configure Databases using DBCA

### Upgrade the Oracle Database

* Plan for Upgrading an Oracle Database
* Upgrade an Oracle Database
* Perform Post-Upgrade tasks

### Using General Overall Database Enhancements

* Install Oracle Database software
* Create, Delete and Configure Databases using DBCA
* Creating CDBs and Regular PDBs
* Use Miscellaneaous 19c New Features

### Using Diagnosibility Enhancements

* Use new Diagnoseability Features

### Tuning SQL Statements

* Understanding The Oracle Optimizer
* Using The SQL Tuning Advisor
* Managing Optimizer Statistics
* Using The SQL Access Advisor
* Understanding The SQL Tuning Process

## REFERENCES:
- Oracle Database: Administration Workshop
- Oracle Database: Introduction to SQL
- Oracle Database: Deploy, Patch and Upgrade Workshop
- Oracle Database: Backup and Recovery Workshop
- Oracle Database: Managing Multitenant Architecture Ed 1
- Oracle Database 19c: New Features for Administrators