# Managing Initialization Parameters Files

## Criando um Server Parameter File

### O exemplo a seguir cria um server parameter file à partir de um arquivo texto do parâmetro de inicialização

CREATE SPFILE FROM PFILE='/home/oracle/initorcl.ora';

### No exemplo abaixo, criamos o spfile fornecendo a localização onde será criado

CREATE SPFILE='/opt/oracle/product/19c/dbhome_1/dbs/spfileorcl.ora' FROM PFILE='/home/oracle/initorcl.ora';

### Temos o exemplo abaixo que ilustra a criação de um SPFILE na localização default utilizando os valores correntes do arquivo de parâmetros de inicialização em memória

startup pfile='/home/oracle/initorcl.ora';
CREATE SPFILE FROM MEMORY;

## Configurando ou alterando os valores de parâmetros de Inicialização

ALTER SYSTEM
SET PROCESSES=1000
COMMENT='alteracao de 900 para 1000 em 02/12/2020'
SCOPE=SPFILE;

## A cláusula SCOPE no comando ALTER SYSTEM SET

• SCOPE = SPFILE
A alteração é aplicada no server parameter file. A alteração não é realizada para a instância corrente. A alteração realizada só será efetivada após o restart da instância.

• SCOPE = MEMORY
A alteração é aplicada em memória somente. A alteração realizada é efetivada imediatamente para a instância corrente para parâmetros dinâmicos. Para parâmetros estáticos, a alteração não é persistente, pois o server parameter não é atualizado.

• SCOPE = BOTH
A alteração é aplicada em ambos, server parameter file e memória. Ou seja, a aplicação para parâmetros dinâmicos tem efeito imediato e para os parâmetros estáticos, será necessário reiniciar a instância. Nesta opção, a alteração de parâmetros é persistente. BOTH é o default.

### Utilize a consulta abaixo para determinar se o parâmetro pode ser alterado dinamicamente ou estaticamente

SQL> select ISINSTANCE_MODIFIABLE from v$parameter where NAME='processes';
ISINS
-----
FALSE

SQL> select ISINSTANCE_MODIFIABLE from v$parameter where NAME='open_cursors';
ISINS
-----
TRUE

## Resetando os valores dos parâmetros de inicialização

select DISPLAY_VALUE from V$SYSTEM_PARAMETER where NAME='open_cursors';

### Para resetar o parâmetro para o valor default

alter system reset open_cursors scope=both;

## Exportando o Server Parameter File

CREATE PFILE='/backup/rman/orcl_initBKP.ora' FROM SPFILE='/opt/oracle/product/19c/dbhome_1/dbs/spfileorcl.ora';

## Métodos de visualização das configurações de parâmetros

Você pode utilizar de diversas formas para checar os valores dos parâmetros de configuração da instância:
• Comandos SHOW PARAMETERS e SHOW SPPARAMETERS;
• Comando CREATE PFILE;
• Além das views como V$PARAMETER, V$SYSTEM_PARAMETER e outras.

## Referências

<https://docs.oracle.com/en/database/oracle/oracle-database/19/admin/creating-andconfiguring-an-oracle-database.html#GUID-5FE92A72-3DA7-4BE9-A137-824ECF8EA05F>
