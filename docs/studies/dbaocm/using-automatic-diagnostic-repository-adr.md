# Using the Automatic Diagnostic Repository (ADR)

## Consultas

SQL> set linesize 300
SQL> COLUMN name FORMAT A25
SQL> COLUMN value FORMAT A65
SQL> SELECT name, value FROM
v$diag_info;

SQL> show parameter diagnostic_dest
SQL> ALTER SYSTEM SET diagnostic_dest='/u01/app/oracle';

## Usando ADRCI

$ adrci
adrci> show homes
adrci> set home diag/rdbms/orcl/orcl

## Exemplos para verificar arquivo de log e trace

adrci> show alert
adrci> show alert -p "message_text like '%incident%'"
adrci> show alert -tail 20
adrci> show alert -p "message_text like '%ORA-%'"
adrci> show alert -tail -f
adrci> show tracefile -t
adrci> show trace /u01/app/oracle/diag/rdbms/orcl/orcl/incident/incdir_72697/orcl_ora_18310_i72697.trc

## Gerenciamento das Informações(Purging Trace Files)

adrci> show control

O campo SHORTP_POLICY de 720 horas(30 dias) aplica-se a :
• Trace files, incluindo todos aqueles contidos nos subdiretórios cdmp_timestamp
• Core dump files
• Packaging Information

O campo LONGP_POLICY, no valor de 8760 horas(365 dias) aplica-se a:
• Incident information
• Incident dumps
• Alert logs

Para alterar:

adrci> set control (SHORTP_POLICY = 72)
adrci> set control (LONGP_POLICY= 2190)
adrci> help purge
adrci> set home diag/rdbms/orcl/orcl

### Set do Oracle Home de escolha

adrci> set home diag/rdbms/orcl/orcl

### Purge manual de acordo com o default

adrci> purge

### Purge manual de trace files de acordo com o default

adrci> purge -type TRACE

### Purge de tudo que é mais velho que 1 mês

adrci> purge -age 43200

### Purge de incidentes que são mais velhos que 1 mês

adrci> purge -age 43200 -type INCIDENT

### Problemas e incidentes

adrci> show alert -p "message_text like '%incident%'"
adrci> show problem
adrci> show incident
adrci> show incident -mode detail -p "incident_id=28794"
adrci> ips create package problem 2 correlate all
adrci> ips generate package 1 in "/tmp"

## Referências

<https://docs.oracle.com/en/database/oracle/oracle-database/19/sutil/oracle-adr-commandinterpreter-adrci.html#GUID-DC5744C7-FAC0-436B-99D5-DBD45B66930B>
<https://docs.oracle.com/en/database/oracle/oracle-database/19/admin/diagnosing-and-resolvingproblems.html#GUID-8DEB1BE0-8FB9-4FB2-A19A-17CF6F5791C3>
