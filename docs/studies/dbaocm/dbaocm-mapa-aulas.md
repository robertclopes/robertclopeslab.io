# OCP 19 - [Introdução](./dbaocm.md)

## Oracle Database Administration I 1Z0-082

### Administration Workshop

-- Dia 1

#### Understanding Oracle Database Architecture

* Understanding Oracle Database Instance Configurations <https://dbaoracle.club.hotmart.com/lesson/LO0Vl3rAeG/understanding-oracle-database-instance-configurations>

* Understanding Oracle Database Memory and Process Structures <https://dbaoracle.club.hotmart.com/lesson/ROxEpb9V4D/understanding-oracle-database-memory>

* Understanding Logical and Physical Database Structures
<https://docs.oracle.com/en/database/oracle/oracle-database/19/cncpt/oracle-database-storage-structures.html#GUID-B679FE3E-8FC1-404C-A368-5A4AC0553664>
<https://dbaoracle.club.hotmart.com/lesson/LO0Vl3rAeG/understanding-oracle-database-instance-configurations>

* Understanding Oracle Database Server Architecture <https://dbaoracle.club.hotmart.com/lesson/0Ov62QjmOj/understanding-oracle-database-server-architecture>

-- Dia 2

#### Managing Database Instances

* Starting Up Oracle Database Instances
<https://dbaoracle.club.hotmart.com/lesson/Xm7YBNvpO6/managing-database-instances>

* Using Data Dictionary Views
<https://docs.oracle.com/en/database/oracle/oracle-database/19/cncpt/data-dictionary-and-dynamic-performance-views.html#GUID-BDF5B748-EB43-4B48-938E-89099069C3BB>

* Shutting Down Oracle Database Instances
<https://dbaoracle.club.hotmart.com/lesson/Xm7YBNvpO6/managing-database-instances>

* Using Dynamic Performance Views
<https://docs.oracle.com/en/database/oracle/oracle-database/19/cncpt/data-dictionary-and-dynamic-performance-views.html#GUID-BDF5B748-EB43-4B48-938E-89099069C3BB>

* Using the Automatic Diagnostic Repository (ADR)
<https://docs.oracle.com/en/database/oracle/oracle-database/21/sutil/oracle-adr-command-interpreter-adrci.html#GUID-DC5744C7-FAC0-436B-99D5-DBD45B66930B>

* Using the Alert Log and Trace Files
<https://docs.oracle.com/en/database/oracle/oracle-database/21/sutil/oracle-adr-command-interpreter-adrci.html#GUID-DC5744C7-FAC0-436B-99D5-DBD45B66930B>

* Managing Initialization Parameter Files
<https://dbaoracle.club.hotmart.com/lesson/14okxWr04p/managing-spfile-(server-parameter-file)>

-- Dia 3

#### Managing Users, Roles and Privileges

* Assigning Quotas to Users
<https://dbaoracle.club.hotmart.com/lesson/meLqqE3Y7n/managing-users-roles-and-privileges-hands-on>

* Applying the Principal of Least Privilege
<https://dbaoracle.club.hotmart.com/lesson/meLqqE3Y7n/managing-users-roles-and-privileges-hands-on>

* Creating and Assigning Profiles
<https://dbaoracle.club.hotmart.com/lesson/meLqqE3Y7n/managing-users-roles-and-privileges-hands-on>

* Administering User Authentication Methods
<https://dbaoracle.club.hotmart.com/lesson/meLqqE3Y7n/managing-users-roles-and-privileges-hands-on>

* Managing Oracle Database Users, Privileges, and Roles
<https://dbaoracle.club.hotmart.com/lesson/meLqqE3Y7n/managing-users-roles-and-privileges-hands-on>

-- Dia 4

#### Managing Storage

* Managing Resumable Space Allocation
<https://dbaoracle.club.hotmart.com/lesson/M7G29xgLew/managing-resumable-space-allocation>

* Shrinking Segments
<https://dbaoracle.club.hotmart.com/lesson/gmeLj05YOn/reclaiming-unused-space>

* Deferring Segment Creation

* Using Space-Saving Features
<https://dbaoracle.club.hotmart.com/lesson/gmeLj05YOn/reclaiming-unused-space>

* Deploying Oracle Database Space Management Features
<https://dbaoracle.club.hotmart.com/lesson/gmeLj05YOn/reclaiming-unused-space>

* Managing Different Types of Segments
<https://dbaoracle.club.hotmart.com/lesson/gmeLj05YOn/reclaiming-unused-space>

* Using Table and Row Compression
<https://dbaoracle.club.hotmart.com/lesson/ZNOwdBq94m/using-table-and-row-compression>

* Understanding Block Space Management
<https://dbaoracle.club.hotmart.com/lesson/3V4VAX09O2/understanding-block-space-management>

-- Dia 5

#### Moving Data

* Using External Tables
<https://dbaoracle.club.hotmart.com/lesson/ZYOmPLQl7d/using-external-tables>

* Using Oracle Data Pump
<https://dbaoracle.club.hotmart.com/lesson/V7y1XxdP7J/external-tables-oracle_datapump-access-driver>
<https://dbaoracle.club.hotmart.com/lesson/V73gXEn273/oracle-data-pump-network_link>
<https://dbaoracle.club.hotmart.com/lesson/37d9j196OL/datapump-parallel-and-compression>

* Using SQL*Loader
<https://dbaoracle.club.hotmart.com/lesson/DPeA1Jk8eW/using-sql*loader>

#### Accessing an Oracle Database with Oracle supplied Tools

* Using the Database Configuration Assistant (DBCA)
<https://dbaoracle.club.hotmart.com/lesson/Go4E6L3mez/oracle-supplied-tools-dbca>

* Using Oracle Enterprise Manager Cloud Control
<https://dbaoracle.club.hotmart.com/lesson/W0OvYnRx7j/oracle-supplied-tools-oem>

* Using Oracle enterprise Manager Database Express
<https://dbaoracle.club.hotmart.com/lesson/V73glBdp73/enterprise-manager-express>

* Using SQL Developer
<https://dbaoracle.club.hotmart.com/lesson/wa4R3LMNen/sql-developer>

* Using SQL Plus
<https://dbaoracle.club.hotmart.com/lesson/0r48qD2weR/oracle-supplied-tools-sql-developer-sql-plus>

-- Dia 6

#### Configuring Oracle Net Services

* Using Oracle Net Services Administration Tools
* Configuring Communication Between Database Instances
* Configuring the Oracle Net Listener
* Connecting to an Oracle Database Instance
* Comparing Dedicated and Shared Server Configurations
* Administering Naming Methods

#### Managing Tablespaces and Datafiles

* Viewing Tablespace Information
<https://dbaoracle.club.hotmart.com/lesson/B146BwoK4d/viewing-tablespace-information-implementing-oracle-managed-files>

* Creating, Altering and Dropping Tablespaces
<https://dbaoracle.club.hotmart.com/lesson/W0OvKMo64j/creating-tablespaces>
<https://dbaoracle.club.hotmart.com/lesson/gmeL65AlOn/altering-and-dropping-tablespaces-managing-table-data-storage>

* Managing Table Data Storage
<https://dbaoracle.club.hotmart.com/lesson/gmeL65AlOn/altering-and-dropping-tablespaces-managing-table-data-storage>

* Implementing Oracle Managed Files
<https://dbaoracle.club.hotmart.com/lesson/B146BwoK4d/viewing-tablespace-information-implementing-oracle-managed-files>

* Moving and Renaming Online Data Files

-- Dia 7

#### Managing Undo

* Understanding Transactions and Undo Data
* Storing Undo Information
* Configuring Undo Retention
* Comparing Undo Data and Redo Data
* Understanding Temporary Undo

### Oracle Database: Introduction to SQL

-- Dia 1

#### Restricting and Sorting Data

* Applying Rules of precedence for operators in an expression
* Limiting Rows Returned in a SQL Statement
* Using Substitution Variables
* Using the DEFINE and VERIFY commands

#### Using Conversion Functions and Conditional Expressions

* Applying the NVL, NULLIF, and COALESCE functions to data
* Understanding implicit and explicit data type conversion
* Using the TO_CHAR, TO_NUMBER, and TO_DATE conversion functions
* Nesting multiple functions

-- Dia 2

#### Displaying Data from Multiple Tables Using Joins

* Using Self-joins
* Using Various Types of Joins
* Using Non equijoins
* Using OUTER joins

#### Using SET Operators

* Matching the SELECT statements
* Using the ORDER BY clause in set operations
* Using The INTERSECT operator
* Using The MINUS operator
* Using The UNION and UNION ALL operators

-- Dia 3

#### Understanding Data Definition Language

* Using Data Definition Language

#### Managing Views

* Managing Views

-- Dia 4

#### Managing Data in Different Time Zones

* Working with CURRENT_DATE, CURRENT_TIMESTAMP,and LOCALTIMESTAMP
* Working with INTERVAL data types

#### Retrieving Data using the SQL SELECT Statement

* Using Column aliases
* Using The DESCRIBE command
* Using The SQL SELECT statement
* Using concatenation operator, literal character strings, alternative quote operator, and the DISTINCT keyword
* Using Arithmetic expressions and NULL values in the SELECT statement

-- Dia 5

#### Using Single-Row Functions to Customize Output

* Manipulating strings with character functions in SQL SELECT and WHERE clauses
* Performing arithmetic with date data
* Manipulating numbers with the ROUND, TRUNC and MOD functions
* Manipulating dates with the date function

#### Reporting Aggregated Data Using Group Functions

* Restricting Group Results
* Creating Groups of Data
* Using Group Functions

-- Dia 6

#### Using Subqueries to Solve Queries

* Using Single Row Subqueries
* Using Multiple Row Subqueries

#### Managing Tables using DML statements

* Managing Database Transactions
* Using Data Manipulation Language
* Controlling transactions

-- Dia 7

#### Managing Sequences, Synonyms, Indexes

* Managing Indexes
* Managing Synonyms
* Managing Sequences

#### Managing Schema Objects

* Creating and using temporary tables
* Managing constraints

## Oracle Database Administration II 1Z0-083

#### Creating CDBs and Regular PDBs

* Configure and create a CDB
* Create a new PDB from the CDB seed
* Explore the structure of PDBs

#### Backup and Duplicate

* Perform Backup and Recover CDBs and PDBs
* Duplicate an active PDB
* Duplicate a Database

#### Manage Application PDBs

* Explain the purpose of application root and application seed
* Define and create application PDBs
* Install, upgrade and Patch applications
* Create and administer Application PDBS
* Clone PDBs and Application containers
* Plug and unplug operations with PDBs and application containers
* Comparing Local Undo Mode and Shared Undo Mode

#### Recovery and Flashback

* Restore and Recovering Databases with RMAN
* Perform CDB and PDB flashback

#### Backup Strategies and Terminology

* Perform Full and Incremental Backups and Recoveries
* Compress and Encrypt RMAN Backups
* Use a media manager
* Create multi-section backups of very large files
* Create duplexed backup sets
* Create archival backups
* Backup of recovery files
* Backup non database files
* Back up ASM meta data

#### Restore and Recovery Concepts

* Employ the best Oracle Database recovery technology for your failure situation
* Describe and use Recovery technology for Crash, Complete, and Point-in-time recovry

#### Using Flashback Technologies

* Configure  your Database  to support Flashback
* Perform flashback operations

#### Duplicating a Database

* Duplicate Databases

#### Diagnosing Failures

* Detect and repair database and database block corruption
* Diagnosing Database Issues

#### Transporting Data

* Transport Data

#### Install Grid Infrastructure and Oracle Database

* Install Grid Infrastructure for a Standalone server
* Install Oracle Database software

#### Patching Grid Infrastructure and Oracle Database

* Patch Grid Infrastructure and Oracle Database

#### Upgrading to Oracle Grid Infrastructure

* Upgrade Oracle Grid Infrastructure

#### Oracle Database 18c: New Features

* Image and RPM based Database Installation

#### Oracle Restart

* Configure and use Oracle Restart to manage components

#### Install Grid Infrastructure for a Standalone server

* Rapid Home Provisioning

#### Using Availability Enhancements

* Use an RMAN recovery catalog
* Use Flashback Database

#### Monitoring and Tuning Database Performance

* Managing Memory Components
* Understanding The Automatic Workload Repository (AWR)
* Understanding The Advisory Framework
* Monitoring Wait Events, Sessions, and Services
* Managing Metric Thresholds and Alerts
* Understanding and Using The Performance Tuning Methodology
* Performing Performance Planning
* Understanding The Automatic Database Diagnostic Monitor (ADDM)

#### Manage CDBs and PDBs

* Manage PDB service names and connections
* Manage startup, shutdown and availability of CDBs and PDBs
* Change the different modes and settings of PDBs
* Evaluate the impact of parameter value changes
* Performance management in CDBs and PDBs
* Control CDB and PDB resource usage with the Oracle Resource Manager

#### Upgrading and Transporting CDBs and Regular PDBs

* Upgrade an Oracle Database
* Transport Data

#### Manage Security in Multitenant databases

* Manage Security in Multitenant databases
* Manage PDB lockdown profiles
* Audit Users in CDBs and PDBs
* Manage other types of policies in application containers

#### Configuring and Using RMAN

* Configure RMAN and the Database for Recoverability
* Configureand Using an RMAN recovery catalog

#### Performing Recovery

* Restore and Recovering Databases with RMAN
* Perform Non RMAN database recovery

#### RMAN Troubleshooting and Tuning

* Interpret the RMAN message output
* Diagnose RMAN performance issues

#### Creating an Oracle Database by using DBCA

* Create, Delete and Configure Databases using DBCA

#### Upgrade the Oracle Database

* Plan for Upgrading an Oracle Database
* Upgrade an Oracle Database
* Perform Post-Upgrade tasks

#### Using General Overall Database Enhancements

* Install Oracle Database software
* Create, Delete and Configure Databases using DBCA
* Creating CDBs and Regular PDBs
* Use Miscellaneaous 19c New Features

#### Using Diagnosibility Enhancements

* Use new Diagnoseability Features

#### Tuning SQL Statements

* Understanding The Oracle Optimizer
* Using The SQL Tuning Advisor
* Managing Optimizer Statistics
* Using The SQL Access Advisor
* Understanding The SQL Tuning Process

## REFERENCES

* Oracle Database: Administration Workshop
* Oracle Database: Introduction to SQL
* Oracle Database: Deploy, Patch and Upgrade Workshop
* Oracle Database: Backup and Recovery Workshop
* Oracle Database: Managing Multitenant Architecture Ed 1
* Oracle Database 19c: New Features for Administrators
