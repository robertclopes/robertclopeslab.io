# Managing Tablespaces

## Viewing information about tablespaces

desc DBA_TABLESPACES

desc v$tablespace

select name from v$tablespace;

select tablespace_name from dba_tablespaces;

select ENCRYPTED from DBA_TABLESPACES;

select ENCRYPT_IN_BACKUP from v$tablespace;

desc DBA_DATA_FILES

desc DBA_FREE_SPACE

### 

set linesize 150

SELECT TABLESPACE_NAME "TABLESPACE",
   INITIAL_EXTENT "INITIAL_EXT",
   NEXT_EXTENT "NEXT_EXT",
   MIN_EXTENTS "MIN_EXT",
   MAX_EXTENTS "MAX_EXT",
   PCT_INCREASE
   FROM DBA_TABLESPACES;

SELECT STATUS, SEGMENT_SPACE_MANAGEMENT
FROM DBA_TABLESPACES
WHERE TABLESPACE_NAME='DBAOCM_TBS';

SELECT FILE_ID, FILE_NAME, ONLINE_STATUS
FROM DBA_DATA_FILES
WHERE TABLESPACE_NAME = 'DBAOCM_TBS'
ORDER BY FILE_ID ASC;

SELECT TABLESPACE_NAME
FROM DBA_TABLES
WHERE OWNER = 'HR'
AND TABLE_NAME - 'JOBS';

## References

<https://docs.oracle.com/en/database/oracle/oracle-database/19/admin/managing-tablespaces.html#GUID-516629D2-D135-45BB-A0B4-46A16DC46798>

<https://docs.oracle.com/en/database/oracle/oracle-database/19/admin/using-oracle-managed-files.html#GUID-4A3C4616-0D81-4BBA-8EAD-FCAA8AD5C15A>
