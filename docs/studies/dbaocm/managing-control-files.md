# Managing Control Files

SQL> show parameter control_files

SQL> ! ls /...

SQL> show parameter db_create_file_dest

## Back Up Control Files

ALTER DATABASE BACKUP CONTROLFILE TO '/oracle/backup/control.bkp';

ALTER DATABASE BACKUP CONTROLFILE TO TRACE;

### Configurando o auto backup do controlfile

$ rman target /
Show all;
CONFIGURE CONTROLFILE AUTOBACKUP ON;
CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR
DEVICE TYPE DISK TO '/backup/rman/cf_%F';

### Gerando o backup do control file via RMAN

$ rman target /
COPY CURRENT CONTROLFILE TO '/backup/rman/control.bkp' ;

### Restaurando o backup do control file via via RMAN

RMAN> RESTORE CONTROLFILE FROM '/backup/rman/control.bkp' ;
RMAN> alter database mount;
RMAN> alter database open reset logs;
RMAN> alter database open;

### Restaurando control file exportando conteúdo existente

SQL> startup nomount;
SQL> create pfile='/home/novoarquivo.ora' from spfile;
vim /home/novoarquivo.ora
    *.control_files='path1','path1'
SQL> shutdown abort;
SQL> startup pfile='/home/novoarquivo.ora'

### Comandos diversos

SQL> select open_mode from v$database;
SQL> select status from v$database;

## Referências

