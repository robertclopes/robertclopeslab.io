# Managing Roles

Uma common role é uma role que é criada no container root; uma local role é uma role criada no PDB.
Em um ambiente multitenant, roles do database podem ser especificadas para um PDB ou utilizadas para todo o container.
Uma common role é uma role que é criada no container root, que será conhecida será conhecida no CDB$root, em todos os PDBs que existem e todos que possam vir a existir.
Uma local role existe somente em um PDB e pode somente ser utilizada dentro desse PDB. Logo, a role não contém qualquer privilégio comumente concedido.

## Algumas notas sobre common e local roles

• Common users podem criar e conceder common roles para outros common users e local users;
• Você pode conceder uma role(local ou common) para um local user ou role somente localmente;
• Se você conceder uma common role localmente, então os privilégios daquela common role aplicam-se somente no container onde a role é concedida;
• Local users não podem criar common roles, mas eles podem conceder o privilégio a common e local users;
• A cláusula CONTAINER=ALL é a default quando você cria uma common role no CDB root ou em um application root.

## Como Funciona a Common Role

• Quem concede o privilégio é um common user.
• Quem concede o privilégio possui comumente o grant ADMIN OPTION para o privilégio que está sendo concedido.
• O comando GRANT contém a cláusula CONTAINER=ALL.

### Criando um common user

alter session set container=cdb$root;
create user c##hr_mgr identified by welcome123;
grant create session to c##hr_mgr container=ALL;

### Concedemos o privilégio somente no PDB(localmente)

alter session set container=pdb1;
grant dba to c##hr_mgr;

Validando o privilégio no CBD$root, onde o usuário não tem a role DBA, ele recebe o erro de que o objeto não existe:

SQL> conn c##hr_mgr/welcome123@orcl
Connected.
SQL> select count(*) from dba_objects;
select count(*) from dba_objects
 *
ERROR at line 1:
ORA-00942: table or view does not exist

### Enquanto que no PDB, onde ele possui o privilégio na role DBA, ele consegue acessar normalmente a view dba_objects

SQL> conn c##hr_mgr/welcome123@pdb1
Connected.
SQL> select count(*) from dba_objects;
 COUNT(*)
----------
 72542

### Para conceder a common role para o common user

alter session set container=cdb$root;
grant dba to c##hr_mgr CONTAINER=ALL;

## Criando uma Common Role

• Você precisa estar no container root;
• Utilize o COMMON_USER_PREFIX(C##);
• Opcionalmente, utilizar a cláusula CONTAINER=ALL.

Se você não mencionar, a common role vai ser criada somente no container root.

alter session set container=cdb$root;
CREATE ROLE c##sec_admin IDENTIFIED BY
password CONTAINER=ALL;

## Criando uma Local Role

• Você precisa estar conectado ao PDB que você deseja criar a role;
• O nome da local role NÃO pode iniciar com o COMMON_USER_PREFIX(C##);
• Você pode incluir o CONTAINER=CURRENT no comando CREATE ROLE para especificar que a role como local. Se você está conectado ao PDB e omitir a cláusula, não há problema, pois a cláusula é implícita;
• Você não pode common roles e local roles com o mesmo nome, no entanto você pode ter uma local role com o mesmo nome em diferentes PDBs.

conn system@pdb1
CREATE ROLE sec_admin CONTAINER=CURRENT;

## Concedendo e Revogando Roles para Common e Local Users

conn system@orcl
GRANT AUDIT_ADMIN TO c##sec_admin CONTAINER=ALL;
conn system@pdb1
GRANT AUDIT_ADMIN TO c##sec_admin
CONTAINER=CURRENT;

REVOKE sec_admin FROM hr CONTAINER=CURRENT;

## Managing User Roles

Uma user role é um grupo nomeado que possui uma coleção de
privilégios que você pode criar e associar a outros usuários.
User roles são úteis em uma variedade de situações, tal como
restringir o uso de DDL.

Roles podem ter as seguintes funcionalidades:

• Uma role pode ser concedida privilégios de objetos e/ou privilégios de sistema.
• Qualquer role pode ser concedida para qualquer usuário do banco de dados.
• Cada role concedida a um usuário é, em um dado momento, habilitada ou desabilitada, utilizando na sessão do usuário o SET ROLE
    Exemplo: set role sec_admin; / set role none;
• Roles podem ser concedidas para outras roles, mas não para ela mesma. Por exemplo, role 1 não pode ser concedida para role 2, se previamente a role 2 foi concedida para role 1.
• Se uma role não é autenticada por senha ou é segurança pela aplicação, então você pode conceder a role  indiretamente para o usuário. Uma concessão indireta de role se dá através de outra role que já tenha sido concedida a este usuário.

## Propriedades das Roles e suas descrições

Em geral, você cria uma role para gerenciar privilégios, pelas seguintes razões:

• Para gerenciar os privilégios para um determinada aplicação no database
• Para gerenciar os privilégios para um grupo de usuários

### Criando uma Role

Você pode criar uma role que é autenticada com ou sem senha. Você também pode criar external(pelo sistema operacional, network, etc) ou global(por um enterprise directory) roles.

Criando uma role com senha:
CREATE ROLE clerk IDENTIFIED BY password;

Criando uma role sem senha:
CREATE ROLE salesclerk;

### Drop Role

Local Role:
alter session set container=pdb1;
DROP ROLE salesclerk;

Common role:
alter session set container=cdb$root;
DROP ROLE c##salesclerk;

## Autorizando um role utilizando o Database

Se você concedeu um role protegida por senha, então você precisa habilitar ou desabilitar a role fornecendo a senha da role, utilizando o comando SET ROLE.

SQL> conn system/oracle@pdb1
Connected.

SQL> CREATE ROLE clerk IDENTIFIED BY password;
Role created.

SQL> conn hr/hr@pdb1
Connected.

SQL> select count(*) from dba_objects;
select count(*) from dba_objects
 *
ERROR at line 1:
ORA-00942: table or view does not exist

SQL> conn system/oracle@pdb1
Connected.

SQL> grant dba to clerk;
Grant succeeded.

SQL> grant clerk to hr;
Grant succeeded.

SQL> conn hr/hr@pdb1
Connected.

SQL> select count(*) from dba_objects;
select count(*) from dba_objects
 *
ERROR at line 1:
ORA-00942: table or view does not exist

SQL> SET ROLE clerk IDENTIFIED BY password;
Role set.

SQL> select count(*) from dba_objects;
 COUNT(*)
----------
 72546

SQL> set role none;
Role set.

SQL> select count(*) from dba_objects;
select count(*) from dba_objects
 *
ERROR at line 1:
ORA-00942: table or view does not exist

## Referências

<https://docs.oracle.com/en/database/oracle/oracle-database/19/dbseg/configuring-privilege-and-role-authorization.html#GUIDF87A4196-186D-4D31-83B4-E097325C3A29>
<https://docs.oracle.com/en/database/oracle/oracle-database/19/dbseg/configuring-privilege-and-role-authorization.html#GUID-BB7E4BCAC1EE-4614-BF36-5D9BFC3BC16E>