# Managing Undo

## Undo Retention

### Para checar e alterar

SQL> show parameter undo_retention
SQL> alter system set undo_retention=1800 scope=both sid='*';

### Para consultar o melhor valor de undo_retention

### dados coletados a cada 10 minutos dos últimos 4 dias

SQL> select to_char(begin_time, 'DD-MON-RR HH24:MI') begin_time, to_char(end_time, 'DD-MON-RR HH24:MI') end_time, tuned_undoretention from v$undostat order by end_time;

## Retention Guarantee

### Para checar se o retention guarantee está habilitado

SQL> select RETENTION from dba_tablespaces where tablespace_name='UNDOTBS1';

### Para alterar

SQL> alter tablespace UNDOTBS1 retention GUARANTEE;

## Undo Management

### Para identificar o undo tablespace default

SQL> show parameter undo_tablespace

### Para identificar o tipo de gerenciamento, se é automático ou manual

SQL> show parameter undo_management

### Para alterar o tamanho do tablepace

SQL> alter system set undo_management=MANUAL;

## Gerenciando o tamanho do tablespace de undo

### altera o tamanho do datafile

ALTER DATABASE DATAFILE '/u04/oradata/ORCL/B109137280D516DDE0530A38A8C05B3B/datafile/o1_mf_undotbs1_hqsvprvm_.dbf' RESIZE 100M;

### Adiciona um novo datafile

alter tablespace UNDOTBS1 add datafile '/u04/oradata/ORCL/B109137280D516DDE0530A38A8C05B3B/datafile/undotbs01_3.dbf' size 10m autoextend on next 10m maxsize 32767m;

### Criar um novo undo tablespace

CREATE UNDO TABLESPACE undotbs_02 DATAFILE '/u04/oradata/ORCL/
B109137280D516DDE0530A38A8C05B3B/datafile/undotbs2_01.dbf' SIZE 2M REUSE AUTOEXTEND ON;

### alterar o undo tablespace default

ALTER SYSTEM SET UNDO_TABLESPACE = undotbs_02;

### drop de um undo tablespace

drop tablespace undotbs_02 including contents and datafiles;

## Gerenciando temporary undo tablespaces

SQL> ALTER SYSTEM SET TEMP_UNDO_ENABLED = TRUE;
SQL> ALTER SYSTEM SET TEMP_UNDO_ENABLED = FALSE;

## Consultas importantes para monitorar o undo tablespace

### Monitora os efeitos da execução das transações na instância corrente do espaço de undo

SQL> SELECT TO_CHAR(BEGIN_TIME, 'MM/DD/YYYY
HH24:MI:SS') BEGIN_TIME,
 TO_CHAR(END_TIME, 'MM/DD/YYYY
HH24:MI:SS') END_TIME,
 UNDOTSN, UNDOBLKS, TXNCOUNT,
MAXCONCURRENCY AS "MAXCON"
 FROM v$UNDOSTAT WHERE rownum <= 144;

### Monitora os segmentos de roll back

SET PAUSE ON
SET PAUSE 'Press Return to Continue'
SET PAGESIZE 60
SET LINESIZE 300
COLUMN username FORMAT A20
COLUMN sid FORMAT 9999
COLUMN serial# FORMAT 99999
SELECT s.username,
 s.sid,
 s.serial#,
 t.used_ublk,
 t.used_urec,
 rs.segment_name,
 r.rssize,
 r.status
FROM v$transaction t,
 v$session s,
 v$rollstat r,
 dba_rollback_segs rs
WHERE s.saddr = t.ses_addr
AND t.xidusn = r.usn
AND rs.segment_id = t.xidusn
ORDER BY t.used_ublk DESC;

### Verificar tablespace

SQL> @tbsn UNDOTBS1

select file_name from dba_data_files where tablespace='UNDOTBS1';

## Referências

https://docs.oracle.com/en/database/oracle/oracle-database/19/admin/managingundo.html#GUID-2C865CF9-A8B5-4BF1-A451-E8C08D3611F0