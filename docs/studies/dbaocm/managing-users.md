# Managing Users

## Sobre Common Users e Local Users

Em ambiente Multitenant, CDB common users podem ter acesso aos containers, enquanto que local users acessam PDBs específicos.
• SYS e SYSTEM são common users.
• Contas de local users não podem criar contas de common users.
• Um common user com os privilégios apropriados pode criar e modificar common ou local users, conceder e revogar privilégios, comumente ou localmente.
• Você pode conceder a local users common roles.
• Com os privilégios apropriados, um local user pode acessar objetos em um common user.
• Local users não podem ter o prefixo C##.

## Criação de um local user com o mínimo de privilégio

ALTER SESSION SET CONTAINER=pdb1;
CREATE USER jward
 IDENTIFIED BY password
 DEFAULT TABLESPACE USERS
 QUOTA 10M ON USERS
 TEMPORARY TABLESPACE temp
 QUOTA 5M ON SYSTEM
 PASSWORD EXPIRE;

GRANT CREATE SESSION TO jward;
conn system/oracle@pdb1
SELECT USERNAME FROM DBA_USERS;

## Criação de um common user com o mínimo de privilégio

ALTER SESSION SET CONTAINER=cdb$root;
CREATE USER __c##jward__
 IDENTIFIED BY password
 DEFAULT TABLESPACE USERS
 QUOTA 10M ON USERS
 TEMPORARY TABLESPACE temp
 QUOTA 5M ON SYSTEM
 PASSWORD EXPIRE
__CONTAINER = ALL__;

GRANT SET CONTAINER, CREATE SESSION TO c##jward
container=ALL;

## Alteração da senha de usuário non-sys

ALTER USER jward IDENTIFIED BY password;

## Alteração da senha de usuário sys

$ cd $ORACLE_HOME/dbs
$ orapwd input_file=orapworcl file=orapworcl sys=y force=y

## Para bloquear um usuário explicitamente

ALTER USER jward ACCOUNT LOCK;

## Para desbloquear

SELECT USERNAME,ACCOUNT_STATUS,LOCK_DATE FROM DBA_USERS;
ALTER USER jward ACCOUNT UNLOCK;

## Dropping Users

Para dropar um usuário, não poder haver nenhuma sessão dele no database. Se houverem conexões, você precisa eliminá-las.

SELECT SID, SERIAL#, USERNAME FROM V$SESSION
WHERE USERNAME='JWARD';

ALTER SYSTEM DISCONNECT SESSION '<sid>, <serial#>' immediate;
ou
ALTER SYSTEM KILL SESSION '<sid>, <serial#>';

Uma vez que as sessões do usuário foram finalizadas, é possível dropar o usuário:

DROP USER jward;

Caso o usuário seja um schema, ou seja, possua objetos, será necessário adicionar a cláusula CASCADE:

SELECT OWNER, OBJECT_NAME FROM DBA_OBJECTS WHERE
OWNER LIKE 'JWARD';
DROP USER jward CASCADE;

## Privilégios de sistema

GRANT SELECT ANY TABLE TO jward;
GRANT DELETE ANY TABLE TO jward;
GRANT EXECUTE ANY PROCEDURE TO jward;
GRANT CREATE TABLE TO jward;
REVOKE CREATE ANY TABLE FROM jward;

## Privilégios de objeto

GRANT SELECT ON HR.EMPLOYEES TO jward;
GRANT DELETE ON HR.REGIONS TO jward;
GRANT INSERT ON HR.COUNTRIES TO jward;

## Diferença entre o privilégio READ e SELECT

O usuário que tiver privilégio READ, poderá somente consultar a tabela, view, view materializada e sinônimos.
Enquanto que o privilégio SELECT, além de abrangir o privilégio READ, poderá bloquear a tabela quando utilizar um comando de SELECT FOR UPADATE, por exemplo.

## Privilégios de objeto Utilizando sinônimos

conn hr/hr@pdb1
CREATE OR REPLACE SYNONYM EMP_SYN FOR EMPLOYEES;

conn system/oracle@pdb1
grant select on hr.employees to jward;
grant select on hr.emp_syn to jward;

conn jward/password@pdb1
select count(*) from hr.employees;
select count(*) from hr.emp_syn;

## Role

É uma coleção de privilégios, seja de sistemas ou de objetos.
CREATE ROLE TESTE;
GRANT SELECT ON HR.EMPLOYEES TO teste;
GRANT EXECUTE ANY PROCEDURE TO teste;
GRANT TESTE TO jward;

## References

<https://docs.oracle.com/en/database/oracle/oracle-database/19/admin/managing-users-and-securing-the-database.html#GUID-6ECD7474-E756-4B3E-B5CF-2B92B1BCACA1>
<https://docs.oracle.com/en/database/oracle/oracle-database/19/dbseg/managing-security-for-oracle-database-users.html>
<https://docs.oracle.com/en/database/oracle/oracle-database/19/dbseg/configuring-privilege-and-role-authorization.html#GUID-89CE989DC97F-4CFD-941F-18203090A1AC>
