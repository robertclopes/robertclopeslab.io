# Managing Database Instances

STARTUP

                * OPEN (database opened for this instance) datafile
            * MOUNT (control file opened for this instance) controlfile
        * NOMOUNT (instance started) spfile
    * SHUTDOWN

FORCE - to start after a startup or shutdown problem.
OPEN RECOVER - start the instance and have complete media recovery begin immediately
RESTRICT - only administrative personnel (RESTRICTED SESSIONS system privilege)
    ALTER SYSTEM DISABLE RESTRICTED SESSION;

ALTER DATABASE OPEN READ ONLY;
ALTER DATABASE OPEN READ WRITE;

$ORACLE_HOME/dbs local onde estará o spfile ou init

SHUTDOWN

                * OPEN (database opened for this instance) datafile
            * CLOSE (control file opened for this instance) controlfile
        * NOMOUNT (instance started) spfile
    * SHUTDOWN

BEHAVIOR    |   abort   |   immediate   |   transactional   | normal
Permits new user connections | No | No | No | No
Waits until current sessions end | No | No | No | Yes
Waits until current transactions end | No | No | Yes | Yes
Performs a checkpoint and closes open files | No | Yes | Yes | Yes

sho parameter pfile

create pfile='/tmp/orcl_pfile.ora' from spfile;

startup pfile='/tmp/orcl_pfile.ora';

sqlpus / sys as sysdba
sqlplus /nolog
rman target /

desc V$INSTANCE

## DATA DICTIONARY

DBA_
ALL_
USER_

select * from DICTIONARY order by TABLE_NAME;

## DIAGNOSTIC FILES

ADR (TRACE, ALERT, DDL LOG)

adrci
> show problem
> show incident
> show alert

sqlplus / as sysdba
> select name, value from v$diag_info;

## PARAMETER FILES

sho parameter pfile
sho parameter MAX_DUMP_FILE_SIZE
sho parameter MEMORY

sho parameter cursors
alter system set open_cursors=400 SCOPE=MEMORY;
    SCOP=SPFILE/BOTH/MEMORY

## References

https://docs.oracle.com/en/database/oracle/oracle-database/19/cncpt/oracle-database-instance.html#GUID-B3A8DB74-211A-453C-8B73-B61824DC56F6

https://docs.oracle.com/en/database/oracle/oracle-database/19/admin/starting-up-and-shutting-down.html#GUID-203404B5-5157-4F29-A241-B8ABC4753819

https://docs.oracle.com/en/database/oracle/oracle-database/19/cncpt/data-dictionary-and-dynamic-performance-views.html#GUID-BDF5B748-EB43-4B48-938E-89099069C3BB

https://docs.oracle.com/en/database/oracle/oracle-database/19/cncpt/oracle-database-instance.html#GUID-E329D228-F334-4506-84C1-EC030032C228

https://docs.oracle.com/en/database/oracle/oracle-database/19/cncpt/oracle-database-instance.html#GUID-BB6ADD85-55FD-42C4-A847-80F0BACDE790

https://docs.oracle.com/en/database/oracle/oracle-database/19/admin/starting-up-and-shutting-down.html#GUID-4D231FF8-F3A2-4BE8-91BC-8A6AC283C490