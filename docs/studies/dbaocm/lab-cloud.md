# Oracle Cloud

## Criar conta

<https://signup.oraclecloud.com/?sourceType=_ref_coc-asset-opcSignIn&language=en_BR>

<https://console.sasaopaulo-1.oraclecloud.com/?tenant=robertclopes>

## Criar Instance

Salvar keys

Converter private key to ppk

### Configure o SSH da Compute Instance

```bash
# vi /etc/ssh/sshd_config
X11Forwarding yes
X11UseLocalhost no
TCPKeepAlive yes
# service sshd restart
```

### Pacotes necessários para interface gráfica

yum install xorg-x11-xauth dbus-x11 @Fonts

### Instale o pacote pré instalação

```bash
# yum -y install oracle-databasepreinstall-19c
# passwd oracle
    0r4cl3@Cloud
# reboot
```

### Instale o pacote RPM

yum -y localinstall oracle-databaseee-19c-1.0-1.x86_64.rpm

### Crie e configure o listener

$ . oraenv
ORACLE_SID = [orcl] ?
ORACLE_HOME = [/home/oracle] ? /opt/oracle/product/19c/dbhome_1
The Oracle base has been changed from
to /opt/oracle
$ netca
$ lsnrctl status

### Crie o banco de dados

Como a compute instance possui apenas 1 GB de memória, precisamos configurar a SGA e PGA com 70% do total de memória do servidor, ou seja, SGA com 600 MB e PGA com 97 MB. Chame o utilitário DBCA para realizar a criação do banco de dados. Processos 900

$ dbca
    0r4cl3@

### Valide o acesso ao banco de dados

$ . oraenv
ORACLE_SID = [orcl] ?
ORACLE_HOME = [/home/oracle] ? /opt/oracle/product/19c/dbhome_1
The Oracle base has been changed from to /opt/oracle
$ sqlplus / as sysdba
set lines 10000 pages 5000
select INSTANCE_NAME, HOST_NAME, VERSION, STARTUP_TIME, STATUS from v$instance;



## Links

Oracle Cloud [https://idcs-57acdc7e0b884577afef8b1b73380960.identity.oraclecloud.com/ui/v1/signin]

<https://www.oracle.com/database/technologies/oracle19c-linuxdownloads.html>

<https://mobaxterm.mobatek.net/download.html>

<https://www.oracle.com/br/cloud/free/>

Requirements for Installing Oracle Database 19c on OL7 or
RHEL7 64-bit (x86-64) (Doc ID 2551169.1)

-- Installing Oracle Database Using RPM Packages
<https://docs.oracle.com/en/database/oracle/oracle-database/19/ladbi/running-rpm-packages-to-install-oracledatabase.html#GUID-BB7C11E3-D385-4A2F-9EAF-75F4F0AACF02>

-- Conectando-se à Sua Instância
<https://docs.cloud.oracle.com/pt-br/iaas/Content/GSG/Tasks/testingconnection.htm>

-- Accessing an Instance from Windows
<https://docs.oracle.com/en/cloud/iaas/compute-iaas-cloud/stcsg/accessing-oracle-linux-instance-using-ssh.html#GUIDE48C21AA-8FCA-4C11-977B-F08B7A419539>
