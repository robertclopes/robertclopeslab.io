# Configuring Privilege and Role Authorization

Verificando permissões

SELECT GRANTEE, GRANTOR, PRIVILEGE, GRANTABLE
 FROM DBA_TAB_PRIVS
 WHERE TABLE_NAME = 'EMPLOYEES' and OWNER = 'HR';

## References

<https://docs.oracle.com/en/database/oracle/oracle-database/19/dbseg/configuringprivilege-and-role-authorization.html#GUID-89CE989DC97F-4CFD-941F-18203090A1AC>
<https://docs.oracle.com/en/database/oracle/oracle-database/19/dbseg/configuringprivilege-and-role-authorization.html#GUID-633842B8-4B19-4F96-A757-783BF62825A7>
<https://docs.oracle.com/en/database/oracle/oracle-database/19/dbseg/configuringprivilege-and-role-authorization.html#GUID-6F401301-B5EA-482E-9615-21FD840CAF60>
<https://docs.oracle.com/en/database/oracle/oracle-database/19/dbseg/configuring-privilege-and-role-authorization.html#GUID-8AB43A10-FD9E-4B08-9C23-71C5A15DE1FC>
<https://docs.oracle.com/en/database/oracle/oracle-database/19/dbseg/configuringprivilege-and-role-authorization.html#GUID-54A52E12-29D6-4AA0-B94CF31E79ED5B16>
