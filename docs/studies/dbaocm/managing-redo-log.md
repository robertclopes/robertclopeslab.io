# Managing Redo Log

## Processos/Conceitos

LGWR - Log Writer
ARCn - Archive Process
SCN - System Change Number

## Queries

select group#, thread# from v$log;

col current_scn for 999999999999999
select current_scn from v$database;

set term on
set linesize 210
set feed off
col file_name for a15 wrap
col first_time for a19
col first_change# for 99999999999999999999
col tam for 9999999.99
col mbytes for 9999999.99
select group#,thread#,sequence#,bytes/1024/1024 mbytes,
 members,archived,status,first_change#,
 to_char(first_time,'dd/mm/yyyy hh24:mi:ss') first_time
from v$log;
select substr(lf.member,instr(lf.member,'\',-1)+
 instr(lf.member,'/',-1)+
 instr(lf.member,']',-1)+1) file_name,
 l.bytes/1024/1024 tam,
 l.thread#, l.status,first_change#,
 to_char(first_time,'dd/mm/yyyy hh24:mi:ss') first_time
from v$logfile lf,v$log l
where lf.group# = l.group#
order by lf.member;

### Forçando Log Switches

alter system switch logfile;

### Controlando o Archive Lag

alter system set archive_lag_target = 1800;

### Criando Redo Log Groups

ALTER DATABASE
    ADD LOGFILE GROUP 4 ('/opt/oracle/
fast_recovery_area/ORCL/onlinelog/group4a.rdo',
'/opt/oracle/oradata/ORCL/onlinelog/
group4b.rdo') SIZE 200M;

### Removendo Redo Log Groups

SELECT GROUP#, ARCHIVED, STATUS FROM V$LOG;

Para dropar o grupo 4, por exemplo:

ALTER DATABASE DROP LOGFILE GROUP 4;

### Removendo Redo Log Members

ALTER DATABASE DROP LOGFILE MEMBER '/opt/oracle/
fast_recovery_area/ORCL/onlinelog/group4a.rdo';

### Clearing um Redo Log File

ALTER DATABASE CLEAR LOGFILE GROUP 3;

ALTER DATABASE CLEAR UNARCHIVED LOGFILE GROUP 3;

### Precedência das configurações do FORCE LOGGING



## Views do Dicionário de Dados para Redo Logs

SELECT GROUP#, THREAD#, SEQUENCE#, BYTES,
MEMBERS, ARCHIVED,
 STATUS, FIRST_CHANGE#, FIRST_TIME
 FROM V$LOG;
SELECT GROUP#, STATUS, MEMBER FROM V$LOGFILE;

## Referências:

https://docs.oracle.com/en/database/oracle/oracle-database/19/admin/managing-theredo-log.html#GUID-BC1F1762-0BB1-4218-B7AF-6160C395AAE4